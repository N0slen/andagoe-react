Meteor.methods({
	'insertLocation': function(lat,lon,timestamp){
		
		//var myUserOrg = Meteor.user();
		//var thisUserOrg = myUserOrg.profile.organization;

		Locations.insert({
			user: this.userId,
		//	organization: thisUserOrg,
			positions: {
				timestamp: timestamp,
				latitude: lat,
				longitude: lon
			},
			createdAt: new Date()
		});
	}
});
