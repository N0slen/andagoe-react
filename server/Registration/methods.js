Meteor.methods({
	'insertCompany': function(fname,lname,email,password,org){
		Accounts.createUser({
		email: email,
		password: password,
			profile: {
				firstname:fname,
				lastname:lname,
				role:"Admin", 
				createdAt: new Date()}
		});

		 var thisUser = Accounts.findUserByEmail(email);
		 if (thisUser) {
		 	Organizations.insert({
				organization: org,
		 		createdAt: new Date(),
		 		Admin:[
		 			thisUser._id,
		 		]
		 	});
		 }

		 var thisOrg = Organizations.findOne({ organization: org});
		 Meteor.users.update({ _id: thisUser._id},{
		 	$set:{
		 		'profile.organization': thisOrg._id 
		 	}
		 });
	},

	'addDriver': function(fname,lname,email,password,role,currentUserId){

		var organization = Organizations.findOne({},{Admin: currentUserId});

		var userId = Accounts.createUser({
			email: email,
			password: password,
			profile: {
				firstname: fname,
				lastname: lname,
				role: role,
				addedBy: currentUserId,
				organization: organization._id
			},
		});
		if (role === 'Admin') {
			Organizations.update({_id: organization._id},{$push: {Admin: userId}});
	}
	},
	'addMessage': function(text,currentUserId,fUser,lUser,time) {
    const message = {
    	user: currentUserId,
    	fName: fUser,
    	lName: lUser,
    	createdAt: new Date(), 
    	time: time,
    	text: text,
    };
	Messages.insert(message);
  }
});