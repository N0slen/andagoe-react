import React, {Component} from 'react';

export default class DriverRegistration extends Component {
	onSubmit(e){
		e.preventDefault();
		var currentUserId = Meteor.userId();
		var tg = $(e.target);
		var fname = tg.find("#fn").val();
		var lname = tg.find("#ln").val();
		var email = tg.find("#email").val();
		var role = $('input[name="options"]:checked').val();
		var password = tg.find("#password").val();
		var passwordConfirm = tg.find("#passwordConfirm").val();
 		if (password===passwordConfirm && password!=="" && passwordConfirm!=="") {
		Meteor.call('addDriver', fname,lname,email,password,role, currentUserId,function(err){
				if (err) {
					console.log(err);
				}else{
				console.log("Successfully added");
				Bert.alert( 'Account created with success!', 'success', 'fixed-top', 'fa-frown-o' );
		tg.find("#fn").val("");
		tg.find("#ln").val("");
		tg.find("#email").val("");
		$('input[name="options"]:unchecked').val();
		tg.find("#password").val("");
		tg.find("#passwordConfirm").val("");				
				}
			})
	}
}
render(){
  $('#myStateButton').on('click', function () {
    $(this).button('complete') 
  });
    Meteor.subscribe('driver');
//POR MODAL QUE SAI DEBAIXO SE DER, PARA VER OS USERS !
return(

<div>
	<form className="form-horizontal" onSubmit={this.onSubmit}>
		<div className="row main">
			</div>
			<div className="main-login main-center">
				<div className="panel-title text-center">
					<h1 className="title">Register</h1>
					<hr />
				</div>
				<div className="form-group">
					<label className="cols-sm-2 control-label">First Name</label>
					<div className="cols-sm-10">
						<div className="input-group">
							<span className="input-group-addon"><i className="fa fa-user fa" aria-hidden="true"></i></span>
							<input id="fn" type="text" className="form-control" placeholder="Enter your First Name"/>
						</div>
					</div>
				</div>
				<div className="form-group">
					<label className="cols-sm-2 control-label">Last Name</label>
					<div className="cols-sm-10">
						<div className="input-group">
							<span className="input-group-addon"><i className="fa fa-user fa" aria-hidden="true"></i></span>
							<input id="ln" type="text" className="form-control" placeholder="Enter your Last Name"/>
						</div>
					</div>
				</div>
				<div className="form-group">
					<label className="cols-sm-2 control-label">Your Email</label>
					<div className="cols-sm-10">
						<div className="input-group">
							<span className="input-group-addon"><i className="fa fa-envelope fa" aria-hidden="true"></i></span>
							<input id="email" type="email" className="form-control" placeholder="Enter your Email"/>
						</div>
					</div>
				</div>
				<div className="btn-group" data-toggle="buttons">
					<label className="btn btn-default">
						<input type="radio" name="options" id="option1" autocomplete="off" value={"Admin"}/>Admin</label>
						<label className="btn btn-default">
							<input type="radio" name="options" id="option2" autocomplete="off" value={"Coordenator"}/>Coordenator</label>
							<label className="btn btn-default">
								<input type="radio" name="options" id="option3" autocomplete="off" value={"Driver"}/>Driver</label>
							</div>
							<div className="form-group">
								<label className="cols-sm-2 control-label">Password</label>
								<div className="cols-sm-10">
									<div className="input-group">
										<span className="input-group-addon"><i className="fa fa-lock fa-lg" aria-hidden="true"></i></span>
										<input id="password" type="password" className="form-control" placeholder="Enter your Password"/>
									</div>
								</div>
							</div>
							<div className="form-group">
								<label className="cols-sm-2 control-label">Confirm Password</label>
								<div className="cols-sm-10">
									<div className="input-group">
										<span className="input-group-addon"><i className="fa fa-lock fa-lg" aria-hidden="true"></i></span>
										<input id="passwordConfirm" type="password" className="form-control" placeholder="Confirm your Password"/>
									</div>
								</div>
							</div>
							<div className="form-group">
								<button className="btn btn-default">Register</button>
							</div>
						</div>
				</form>
			</div>
)}
} 