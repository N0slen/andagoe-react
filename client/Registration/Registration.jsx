import React, {Component} from 'react';

export default class Registration extends Component {

	onSubmit(e){
		e.preventDefault();

		var tg = $(e.target);
		var fname = tg.find("#fn").val();
		var lname = tg.find("#ln").val();
		var email = tg.find("#email").val();
		var org = tg.find("#comp").val();
		var password = tg.find("#password").val();
		var passwordConfirm = tg.find("#passwordConfirm").val();
 		if (password===passwordConfirm && password!=="" && passwordConfirm!=="") {
			
			Meteor.call('insertCompany', fname,lname,email,password, org,function(err){
				if (err) {
					console.log(err);
				}
				else {
					Meteor.loginWithPassword(email, password)
					console.log("Successfully added");
					tg.find("#fn").val("");
					tg.find("#ln").val("");
					tg.find("#email").val("");
					tg.find("#comp").val();	
					tg.find("#password").val("");
					tg.find("#passwordConfirm").val("");	
					FlowRouter.go("/Drivers");
				};
			});
		}
	}

render(){
	return (
			<div>
					<form className="form-horizontal" onSubmit={this.onSubmit}>
			<div className="row main">
				<div className="panel-heading">
	               <div className="panel-title text-center">
	               		<h1 className="title">Register</h1>
	               		<hr />
	               	</div>
	            </div> 
				<div className="main-login main-center">
						
						<div className="form-group">
							<label className="cols-sm-2 control-label">First Name</label>
							<div className="cols-sm-10">
								<div className="input-group">
									<span className="input-group-addon"><i className="fa fa-user fa" aria-hidden="true"></i></span>
									<input id="fn" type="text" className="form-control" placeholder="Enter your First Name"/>
								</div>
							</div>
						</div>

						<div className="form-group">
							<label className="cols-sm-2 control-label">Last Name</label>
							<div className="cols-sm-10">
								<div className="input-group">
									<span className="input-group-addon"><i className="fa fa-user fa" aria-hidden="true"></i></span>
									<input id="ln" type="text" className="form-control" placeholder="Enter your Last Name"/>
								</div>
							</div>
						</div>						

						<div className="form-group">
							<label className="cols-sm-2 control-label">Your Email</label>
							<div className="cols-sm-10">
								<div className="input-group">
									<span className="input-group-addon"><i className="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input id="email" type="email" className="form-control" placeholder="Enter your Email"/>
								</div>
							</div>
						</div>

						<div className="form-group">
							<label className="cols-sm-2 control-label">Company</label>
							<div className="cols-sm-10">
								<div className="input-group">
									<span className="input-group-addon"><i className="fa fa-users fa" aria-hidden="true"></i></span>
									<input id="comp" type="text" className="form-control" placeholder="Enter your Company"/>
								</div>
							</div>
						</div>

						<div className="form-group">
							<label className="cols-sm-2 control-label">Password</label>
							<div className="cols-sm-10">
								<div className="input-group">
									<span className="input-group-addon"><i className="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input id="password" type="password" className="form-control" placeholder="Enter your Password"/>
								</div>
							</div>
						</div>

						<div className="form-group">
							<label className="cols-sm-2 control-label">Confirm Password</label>
							<div className="cols-sm-10">
								<div className="input-group">
									<span className="input-group-addon"><i className="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input id="passwordConfirm" type="password" className="form-control" placeholder="Confirm your Password"/>
								</div>
							</div>
						</div>

						<div className="form-group">
							<button className="btn btn-default">Register</button>
						</div>
				</div>
			</div>
		</form>
	</div>
	)}
} 