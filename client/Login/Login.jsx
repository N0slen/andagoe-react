import React, {Component} from 'react';

export default class Login extends Component {
	
onSubmit(e){
    e.preventDefault();
    var ee = $(e.target);
    var email = ee.find("#email").val();
    var password = ee.find("#password").val();

    Meteor.loginWithPassword(email, password, (er)=>{
      if (er) {
        Bert.alert( 'Email ou Password incorreto!', 'danger', 'fixed-top', 'fa-frown-o' );
      }else{
        Bert.alert( 'Login feito com sucesso!', 'success', 'growl-top-left' );
      }

    })
  }
	render(){
  var navStyle={
	paddingLeft: "20px",
  paddingRight: "20px",
};	
		return(
			<li>
        <form onSubmit={this.onSubmit} className="">

          <div className="col-md-12">
            <label htmlFor="email">Email</label>
            <input id="email" type="email" className="validate form-control"/>
      
            <label htmlFor="password">Password</label>
            <input id="password" type="password" className="validate form-control"/>
            
            <li role="separator" className="divider"></li>
            <button className="btn btn-primary" id="bigBtn" type="submit">Login</button>
          </div>

        </form>
      </li>
    )
  }
}