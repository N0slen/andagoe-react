import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

import Login from '../Login/Login.jsx';
import NavigationPrivate from './NavigationPrivate.jsx';
import NavigationPublic from './NavigationPublic.jsx';


export default class Navigation extends TrackerReact(Component) {
    
 render(){
var isUserLogged;
        if (Meteor.userId()) {
            isUserLogged = <NavigationPrivate />
        }
        else {
            isUserLogged = <NavigationPublic />
        }
    return(

<nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand whiteText" href="/">AndaGoe</a>
                    </div>
                        {isUserLogged}
                </div>
            </nav>

    )
  }
}
