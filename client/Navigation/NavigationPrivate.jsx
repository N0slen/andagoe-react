import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class NavigationPrivate extends TrackerReact(Component) {
    handleLogout(){
        Meteor.logout((err)=>{
            if (err) {
                Bert.alert( 'Could not Logout', 'danger', 'fixed-top', 'fa-frown-o' );
            } 
            else {
                Bert.alert( 'Logged Out', 'success', 'fixed-top', 'fa-frown-o' );
            }
        });
    }
    render() {
        var currentUserId = Meteor.userId();
        var currentUser = Meteor.users.find({_id: currentUserId}).fetch();
        var User = currentUser.map(function(user, key){
        return <a href="#" key={user._id} className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{user.profile.firstname} <span className="caret"></span></a>
        });
        return (
            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul className="nav navbar-nav navbar-right">
                    <li><a href="#">About</a></li>
                    <li><a href="/Drivers">Drivers</a></li>
                    <li className="dropdown">
                        {User}
                        <ul className="dropdown-menu">
                            <li role="" className=""> <a href="/Locations"> Locations </a> </li>
                            <li role="separator" className="divider"></li>
                            <li style={{paddingLeft:"48px"}}>
                            <span type="button" className="btn btn-default btn-sm" onClick={this.handleLogout}>
                                <span className="glyphicon glyphicon-log-out"></span> Log out
                             </span>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        )
    }
}