import React, {Component} from 'react';
import Login from '../Login/Login.jsx';


export default class Navigation extends Component {
    render() {
        
        return (        
                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul className="nav navbar-nav navbar-right">
                            <li><a href="#">About</a></li>
                            <li className="dropdown">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Login<span className="caret"></span></a>
                                <ul className="dropdown-menu">
                                    <Login />
                                    <li role="separator" className="divider"></li>
                                    <li><a href="/Registration">Ask for invite</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
        )
    }
}