import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import moment from 'moment';

export default class Chat extends TrackerReact(Component) {
handleSubmit(event) {
    event.preventDefault();
    
    var tg = $(event.target);
    var text = tg.find("#textInput").val();
    var currentUserId = Meteor.userId();
		var currentUser = Meteor.users.find({_id: currentUserId}).fetch();
		var fUser = currentUser.map(function(user, key){
		return user.profile.firstname
		});
		var currentUser = Meteor.users.find({_id: currentUserId}).fetch();
		var lUser = currentUser.map(function(user, key){
		return user.profile.lastname
		});	
    	var newTime = new Date();  
    	var time = newTime.toUTCString();
    Meteor.call('addMessage', text,currentUserId,fUser,lUser,time);

    tg.find("#textInput").val("");
  }
    render() {
    	  $(function(){
			$("#addClass").click(function () {
          $('#qnimate').addClass('popup-box-on');
            });
          
            $("#removeClass").click(function () {
          $('#qnimate').removeClass('popup-box-on');
            });
  })  
		var allmessages = Messages.find({}, {sort: { createdAt: -1}});
		var allMessages = allmessages.map(function(message, key){
		return(
			<ul key={message._id} className="chat">
				<div style= {{ maxWidth: "295px", wordWrap: "break-word"}} className="chat-body clearfix">
					<div className="header">
						<strong className="primary-font">{message.fName} {message.lName}</strong><small className="pull-right text-muted">
						<span className="glyphicon glyphicon-time"> {message.time}</span></small>
					</div>
					<p className="list-group-item">{message.text}</p>
				</div>
			</ul>
			)
		});
        return (
			<div className="row">
				<div className="col-md-5">
					<div className="panel panel-primary">
						<div className="panel-heading" id="accordion">
							<span className="glyphicon glyphicon-comment"></span> Chat
							<div className="btn-group pull-right">
								<a type="button" className="btn btn-default btn-xs" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
									<span className="glyphicon glyphicon-chevron-down"></span>
								</a>
							</div>
						</div>
						<div className="panel-collapse collapse" id="collapseOne">
							<div className="panel-body">
							{allMessages}
			</div>
			<form onSubmit={this.handleSubmit}>
			<div className="panel-footer">
				<div className="input-group">
					<input id="textInput" type="text" className="form-control input-sm" placeholder="Type your message here..." />
					<span className="input-group-btn">
						<button className="btn btn-warning btn-sm" id="btn-chat">
						Send</button>
					</span>
				</div>
			</div>
			</form>
			</div>
			</div>
			</div>
			</div>
    )}
};
