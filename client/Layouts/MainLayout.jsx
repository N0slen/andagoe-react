import React from 'react';
import Navigation from '../Navigation/Navigation.jsx';
import Footer from '../Footer/Footer.jsx';

export const MainLayout = ({content}) => (

    <div className="main-layout" >
        <header>
            <Navigation />
        </header>
        <main className="container">
            {content}
        </main>
        	<Footer/>
    </div>
)