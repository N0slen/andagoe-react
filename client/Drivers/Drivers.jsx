import React, {Component} from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import DriverRegistration from '../Registration/DriverRegistration.jsx';
import Chat from '../Chat/Chat.jsx';

export default class Drivers extends TrackerReact(Component) {
	render(){
		var allusers = Meteor.users.find({}).fetch();
		var allUsers = allusers.map(function(user, key){
		return <li key={user._id} className="list-group-item">{user.profile.firstname} {user.profile.lastname}</li>
		});
		var currentUserId = Meteor.userId();
		var currentUser = Meteor.users.find({_id: currentUserId}).fetch();
		var User = currentUser.map(function(user, key){
		return <li key={user._id} className="list-group-item">{user.profile.firstname} {user.profile.lastname}</li>
		});
		return(
<div>
	<Chat/>
	<ul style={{paddingTop:"20px"}} className="list-group">
		{allUsers}
	</ul>
<button type="button" className="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm"><span className="glyphicon glyphicon-new-window"></span>Add Driver</button>
	<div className="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		<div className="modal-dialog modal-sm">
			<div className="modal-content">
				<DriverRegistration/>
			</div>
		</div>
	</div>
</div>
		)
	}
}