import React, {Component} from 'react';
import {greatPlaceStyle} from './my_great_place_styles.js';


export default class MyGreatPlace extends Component {

	render() {

		_onClick = ({x, y, lat, lng, event}) => {
			//x,y,lat,lng only work on googleMap component
			console.log(x, y, lat, lng, event);

			//create/open modal(new component) whenver this event occurres
			/* #myP - create a modal within a component with prop disabled and show onClick */

			$('#myP').text('whatever');

		}

		return (
		   <div onClick={_onClick} style={greatPlaceStyle}>
		   <p id="myP"></p>
		   </div>
		);
	}
}
