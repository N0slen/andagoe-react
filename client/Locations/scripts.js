// Meteor.startup(() => {
// 	console.log('it wordekkk');
	
// });

export function getLocation (){

	//try to start an interval with a function that getCoords and insertCoords to mongo

	navigator.geolocation.getCurrentPosition(
		//param1 getPosition
		function (position){
			var geoPosition = position;

			if (geoPosition !== null) {
				var lat = geoPosition.coords.latitude;
				var lon = geoPosition.coords.longitude;
				var timestamp = geoPosition.timestamp;
				// $('#lat').text(lat);
				// $('#lon').text(lon);
				
				console.log(position);
				if (position) {
					Meteor.call('insertLocation',lat, lon, timestamp);
					console.log('*Inserted*  : ' + position);
					// Meteor.call('consoleError','Location Inserted');
				}
			}
		},
		//param2 - if error
		function(PositionError){
			if (PositionError) {
				// Materialize.toast('Check GPS connection',3000,'error');
				//Client error shown on server cosole
				console.log(PositionError);
				// Meteor.call('consoleError', "positionError Code");
				// Meteor.call('consoleError', PositionError.code);
				// Meteor.call('consoleError', "positionError message");
				// Meteor.call('consoleError', PositionError.message);
			};
		},
		//param3 - options
		{
			maximumAge: 0, 
			enableHighAccuracy: true, 
			timeout:10000
		}
	);
};