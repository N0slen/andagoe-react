
import React, {PropTypes,Component} from 'react';
import GoogleMap from 'google-map-react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import MyGreatPlace from './my_great_place.jsx';
import {getLocation} from './scripts.js';

export default class SimpleMapPage extends TrackerReact(Component) {
    constructor() {
        super();
        const subscription = Meteor.subscribe('locations');
        this.state = {
          ready: subscription.ready(),
          subscription: subscription
        }
    }

  locations(){
    //Meteor.subscribe('locations');
    return Locations.find({}, {sort: {createdAt: -1}}).fetch();
  }

  render() {

    var zoomMap = 15;
    var centerMap = {lat: 37.02235152, lng: -7.93812395};
    
    var lat;
    var lng;
    var key;
    var localClick;
    var myLocals;
    //this.locations();

    myLocals = this.locations();
   

    if (myLocals.length < 1) {
      console.log(myLocals);
      //return null;
    }
    else {
      lat = myLocals[0].positions.latitude;
      lng = myLocals[0].positions.longitude;
      key = myLocals[0]._id;
      console.log(myLocals);
    }

    //startInterval 10000
    var interval = Meteor.setInterval(getLocation,9999);
      //console.log(interval);

    //when click on marker get marker user and show info (userinfo,next order, etc...)
    // _onClick = ({x, y, lat, lng, event}) => {
    //   console.log(x, y, lat, lng, event);

    //   //create/open modal(new component) whenver this event occurres
    //   mouseX = x;
    //   mouseY = y;
    //   $('#myP').text(mouseX + "  " + mouseY);
    // }

    return (
      <div className="map-container">
        <GoogleMap
          defaultZoom={zoomMap}
          defaultCenter={centerMap}
          bootstrapURLKeys={{key:'AIzaSyA1Lc8hDEBFd88R96eQX1bq67ILIWvXwrg'}}
        >
          <MyGreatPlace lat={lat} lng={lng} />
        </GoogleMap>
      </div>
    )
  }
}
