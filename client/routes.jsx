import React from 'react';
import {mount} from 'react-mounter';
import {MainLayout} from './Layouts/MainLayout.jsx';
import Home from './Home/Home.jsx';
import Registration from './Registration/Registration.jsx';
import SimpleMapPage from './Locations/Locations.jsx';
import Drivers from './Drivers/Drivers.jsx';

FlowRouter.route("/",{
  action(params){
        mount( 
            MainLayout,{ 
                content: (<Home />) 
            }
        )
  }
});

FlowRouter.route("/Registration",{
    action(params){
            mount( 
            MainLayout,{ 
                content: (<Registration />) 
            }
        )
    }
});

FlowRouter.route("/Drivers",{
    triggersEnter: [ function() { 
    Meteor.subscribe('driver');
    Meteor.subscribe('messages'); 
  }], 
  action(params){
    mount( 
            MainLayout,{ 
                content: (<Drivers />) 
            }
        )
}
});

FlowRouter.route('/Locations',{
    action(){
         mount( 
            MainLayout,{ 
                content: (<SimpleMapPage />) 
            }
        )
     }
});